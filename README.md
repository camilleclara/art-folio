# PORTFOLIO 

This is my portfolio website, made using a webpack Seed for Javascript and SASS

## Good To Know
NPM Last updated: Sept. 2019

Clean and classic webpack + eslint (airbnb rules) + babel + file loader scaffolding for real world ES6+ projects.

`npm install` at your first use, then

`npm start` to develop your application

