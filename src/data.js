export const mySkills = [
  {
    name: 'Javascript',
    type: 'Language',
    complementary: ['Node.js', 'React', 'Angular', 'TypeScript'],
    score: 5,
  },
  {
    name: 'C#',
    type: 'Language',
    complementary: ['ASP.NET', 'MVC', 'OOP', 'Miscrosoft SQL Server'],
    score: 3,
  },
  {
    name: 'PHP',
    type: 'Language',
    complementary: ['Symfony', 'MVC', 'OOP'],
    score: 4,
  },
  {
    name: 'IDE',
    type: 'Tools',
    complementary: ['Visual Studio', 'NetBeans', 'Visual Studio Code', 'Brackets'],
    score: 3,
  },
  {
    name: 'Project Management',
    type: 'Tools',
    complementary: ['Scrum', 'Rad', 'Balsamiq', 'FreeMind'],
    score: 2,
  },
];
