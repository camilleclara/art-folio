
import { mySkills } from './data';


const skillsDiv = document.getElementById('skillsDiv');
const menu = document.getElementById('menu');
const skills = document.getElementById('skills');
const backSkills = document.getElementById('backSkills');
function hide(element) {
  element.style.display = 'none';
}
function display(element) {
  element.style.display = 'grid';
}
function buildSkillCards() {
  console.log(mySkills);
}
skillsDiv.addEventListener('click', () => {
  hide(menu);
  display(skills);
  buildSkillCards();
});
